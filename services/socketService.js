const user = require('../models/UserModel');
const chatId = require('../models/ChatModel');
const mssgModel = require('../models/MessageModel');
const orderModel= require('../models/OrderModel');
const {
    Types: {
        ObjectId
    }
} = require('mongoose');
module.exports = async (io) => {
    io.on('connection', (socket) => {
        console.log('a user connected', socket.id);
        socket.emit('message', "welcome");
        // User came online and joined their own room
        socket.on("onlineuser", (data) => {
            console.log("user is online now");
            socket.join(data.userId);
            io.to(data.userId).emit("onlineUserOk", {
                status: 200,
                message: "Ok"
            });
        });

        //user wants to connect with another user
        socket.on('join_room', async (data) => {
            try {
                const {
                    userId,
                    storeId
                } = data;
                if (!userId || !storeId || !ObjectId.isValid(userId) || !ObjectId.isValid(storeId)) {
                    console.log("Invalid sender or receiver ID");
                    return;
                }

                const sender = await user.findOne({
                    _id: userId
                });
                const receiver = await user.findOne({
                    _id: storeId
                });
                if (!sender || !receiver) {
                    console.log("User or receiver id not found");
                    return;
                }
                let room;
                let findRoom = await chatId.findOne({
                    userId: userId,
                    storeId: storeId
                });
                if (!findRoom) {
                    findRoom = await chatId.findOne({
                        userId: storeId,
                        storeId: userId
                    });
                }
                if (!findRoom) {
                    findRoom = await chatId.create({
                        userId: userId,
                        storeId: storeId
                    });
                }
                room = findRoom._id;
                socket.join(room);
                ///////////////////////////////
                /////  to count number of users in a room //////
                const roomInfo = io.sockets.adapter.rooms[room];
                let numUsersInRoom = 0;
                if (roomInfo) {
                    numUsersInRoom = roomInfo.length; // Number of sockets in the room
                }
                console.log(roomInfo);
                console.log(`Number of users in room ${room}: ${numUsersInRoom}`);
                //////////////////////////////
                console.log("User joined roomId:", room);
                io.sockets.in(room).emit('message', {
                    text: 'Is now connected',
                    room: room
                });
            } catch (error) {
                console.error("Error joining room:", error);
            }
        });

        //user want to send message to connected user
        socket.on('send_message', async (data) => {
            console.log("Message received from user", data.userId, "in room", data.room, ":", data.message);
            const {
                room,
                userId,
                storeId,
                message
            } = data;
            if (!room || typeof room !== 'string') {
                console.log("Invalid room ID");
                return;
            }
            const findRoom = await chatId.findOne({
                _id: data.room
            });
            if (!findRoom) {
                console.log("Invalid room ID");
                socket.emit("message", "Invalid room ID");
                return;
            }
            await mssgModel.create({
                room,
                userId,
                storeId,
                message
            });
            io.sockets.in(room).emit('message', {
                text: message
            });
            console.group("message sent");


        });

        //user leaves the room
        socket.on('leave_room', (data) => {
            console.log("User", data.userId, "leaving room:", data.room);
            const room = data.room;
            if (!room) {
                console.log("Room ID is undefined.");
                return;
            }
            socket.leave(room);
            io.sockets.in(room).emit('message', {
                text: 'Has left the room',
                room: room
            });
        });

        // user disconnected from server
        socket.on("disconnect", () => {
            console.log("User disconnected", socket.id);
        });

    });
};