const nodemailer = require('nodemailer');
require('dotenv').config();
const senderEmail = process.env.SENDER_EMAIL;
const senderPassword = process.env.SENDER_PASSWORD;
const replyMail = 'fullstacktester@yopmail.com';

module.exports.sendEmail = async (recipientEmail) => {
    try {

        const transporter = nodemailer.createTransport({
            service: 'gmail',
            auth: {
                user: senderEmail,
                pass: senderPassword
            }
        });

        const mailOptions = {
            from: senderEmail,
            to: recipientEmail,
            subject: "ResultSheet",
            text: "Hey congratulations, You have topped in this exam",
            replyTo: replyMail
        };

        // Send email
        const info = await transporter.sendMail(mailOptions);
        console.log("Email sent to: " + recipientEmail + " ,from:" + senderEmail, info.response);
        return info;
    } catch (error) {
        console.error("Error sending email:", error);
        throw error;
    }
};