require('dotenv').config();
const port = process.env.PORT || 3100;

const http = require('http');
const express = require('express');
const app = express();
const server = http.createServer(app);
const io = require('socket.io')(server);
const swaggerUi = require("swagger-ui-express");
const YAML = require("yamljs");

const v1Routes = require("./v1/routes/index");
const services = require("./services/index");
const connection = require('./Common/Connection');
const cron = require("./cron/cronjobs");
const swaggerDocument = YAML.load('./swagger/collection.yml');


app.use(express.json());
app.use(express.urlencoded({
    extended: true
}));
app.use('/api-docs', swaggerUi.serve, swaggerUi.setup(swaggerDocument));
app.use("/api/v1", v1Routes);

app.use((req, res) => {
    res.status(404).send("Page not found");
});

connection.then(() => {
    server.listen(port, () => {
        console.log(`listening on port ${port}`);
        cron.startCronJobs();
        services.Swagger.createSwagger();
        services.SocketService(io);

    });
}).catch(() => {
    console.log("Error in connecting with database");
});