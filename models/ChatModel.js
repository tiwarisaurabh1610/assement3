const mongoose = require('mongoose');
const chatSchema = new mongoose.Schema({
    senderId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserCollection'
    },
    receiverId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserCollection'
    }
});
const chatModel = new mongoose.model('Chats', chatSchema);
module.exports = chatModel;