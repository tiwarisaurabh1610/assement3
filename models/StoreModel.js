const mongoose = require('mongoose');
const bcrypt = require('bcryptjs');
const Store_schema = new mongoose.Schema({
    storeName: {
        type: String,
        required: true
    },
    Location: {
        type: String,
        required: true
    },
    image: {
        type: String,
        default: ""
    },
    email: {
        type: String,
        required: true
    },
    dialCode: {
        type: String,
        required: true
    },
    mobileNo: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true,
        select: false
    },
    jti: {
        type: String,
        default: ""
    },
    isDeleted: {
        type: Boolean,
        default: false
    }

});

Store_schema.pre("save", async function (next) {
    if (this.isModified("password") || this.isNew) {
        console.log("current password is ", this.password);
        this.password = await bcrypt.hash(this.password, 10);
        console.log("new password is ", this.password);
    }
    next();
});
const Store_Model = new mongoose.model('StoreCollection', Store_schema);

module.exports = Store_Model;