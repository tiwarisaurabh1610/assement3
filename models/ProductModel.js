const mongoose = require('mongoose');

const Product_schema = new mongoose.Schema({
     storeId: {
          type: mongoose.Schema.Types.ObjectId,
          ref: 'StoreCollection',
          required: true
     },
     itemName: {
          type: String,
          required: true
     },
     itemPrice: {
          type: Number,
          required: true
     },
     isAvailable: {
          type: Boolean,
          default: true
     }
});
const Product_Model = new mongoose.model('ProductCollection', Product_schema);

module.exports = Product_Model;