module.exports = {
    Store: require("./StoreModel"),
    User:require('./UserModel'),
    Product:require('./ProductModel'),
    Chats:require('./ChatModel'),
    chatMssg:require('./MessageModel'),
    Order:require('./OrderModel')
};