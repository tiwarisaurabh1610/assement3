const mongoose=require('mongoose');
const mssgSchema = new mongoose.Schema({
    roomId:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'chats'
    },
    message:{
        type:String
    },
    senderId:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'UserCollection'
    },
    receiverId:{
        type: mongoose.Schema.Types.ObjectId, 
        ref: 'UserCollection'
    }

});
const mssgModel=new mongoose.model('message',mssgSchema);
module.exports=mssgModel;