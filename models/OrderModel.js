const mongoose = require('mongoose');
const Order_schema = new mongoose.Schema({
    storeId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'StoreCollection',
        required: true
    },
    items: [{
        type: mongoose.Schema.Types.ObjectId,
        ref: 'ProductCollection'
    }],
    totalPrice: {
        type: Number,
        required: true
    },
    userId: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'UserCollection',
        required: true
    },
    paymentMode: {
        type: String,
        required: true
    },
    orderTime: {
        type: Date,
        default: Date.now
    }
});
const Order_Model = new mongoose.model('OrderCollection', Order_schema);

module.exports = Order_Model;