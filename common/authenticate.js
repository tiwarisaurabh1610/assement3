const jwt = require('jsonwebtoken');

module.exports.generateToken = (data) => {
  return jwt.sign(data, process.env.SECRET_KEY, {
    expiresIn: "2h"
  }); // expires in 24 hours
};

module.exports.verifyToken = (req, res, next) => {
  const token = req.headers['authorization'];
  if (!token) {
    return res.status(403).send({
      auth: false,
      message: 'No token provided.'
    });
  }
  jwt.verify(token, process.env.SECRET_KEY, (err, decoded) => {
    if (err) {
      return res.status(500).send({
        auth: false,
        message: 'Failed to authenticate token.'
      });
    }
    console.log(decoded);
    //req.userId = decoded.id;
    next();
  });
};