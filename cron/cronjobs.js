const Agenda = require("agenda");
const services = require('../services');
let agenda;
agenda = new Agenda({
  db: {
    address: "mongodb://localhost:27017/SocialMedia"
  }
});

module.exports.startCronJobs = async () => {
  console.log("Agenda Started");
  await agenda.start();
};
agenda.define("sendEmail", async (job) => {
  let jobData = job.attrs.data;
  console.log(jobData.email, " is sender Email");
  services.EmailService.sendEmail(jobData.email); 
  job.remove(function (err) {
    if (!err)
      console.log("Successfully removed job from collection");
  });
});

process.on("signUp", async (payload) => {
  payload = JSON.parse(JSON.stringify(payload));
  await agenda.schedule(
    //new Date(moment().add(5, "s")),"sendEmail",payload
    '20 seconds', "sendEmail", payload
  );
  console.log("email sent");
});