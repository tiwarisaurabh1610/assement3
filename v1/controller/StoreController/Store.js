const Model = require("../../../models/index");
const Auth = require("../../../common/authenticate");
const bcrypt = require('bcryptjs');
const validater = require('../../validations');

// Store signup
module.exports.insertStore = async (req, res) => {
    try {
        const {
            error,
            value
        } = validater.Store.storeSignup.validate(req.body);
        if (error) {
            console.log(error.message || error);
            return res.status(400).send(error.message || error);
        }
        console.log(value);

        const existingStoreByEmail = await Model.Store.findOne({
            email: req.body.email,
            isDeleted: false
        });
        const existingStorerByMobile = await Model.Store.findOne({
            dialCode: req.body.dialCode,
            mobileNo: req.body.mobileNo,
            isDeleted: false
        });
        if (existingStoreByEmail || existingStorerByMobile) {
            return res.status(400).send("Already registered with the given email or number");
        }

        const data = await Model.Store.create(req.body);
        console.log(data);

        process.emit('signUp', data); // schedule welcome email on signup
        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
};

// Store login
module.exports.storeLogin = async (req, res) => {
    try {
        console.log("Hello inside login");
        const {
            error,
            value
        } = validater.Store.storeLogin.validate(req.body);
        console.log(value);
        if (error) {
            console.log(error);
            return res.status(400).send(error.details.map(detail => detail.message));
        }
        const {
            email,
            password,
            mobileNo
        } = req.body;
        let store = null;
        if (email) {
            store = await Model.Store.findOne({
                email: email,
                isDeleted: false
            }).select('password');
        } else if (mobileNo) {
            store = await Model.Store.findOne({
                mobileNo: mobileNo,
                isDeleted: false
            }).select('password');
        } else {
            throw new Error("Email or mobile number is required");
        }
        if (store == null) {
            throw new Error("Oops store not found");
        }

        const isMatch = await bcrypt.compare(password, store.password);
        console.log(password);
        console.log(store.password);
        console.log(isMatch);
        if (isMatch) {
            store.jti = (Math.floor(Math.random() * 9000000000) + 1000000000).toString();
            console.log("jit is:", store.jti);
            await store.save();
            ////  here jwt token passed as generateToken as returned by function
            const token = Auth.generateToken({
                "id": store._id,
                role: "Store"
            });
            return res.status(200).json({
                message: "Successful login",
                "token": token
            });
        } else {
            return res.status(401).send("Wrong password");
        }
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// Create products 
module.exports.createProducts = async (req, res) => {
    try {
        await validater.Product.createProd.validate(req.body);
        const findStore = await Model.Store.findOne({
            _id: req.params.id
        });
        if (!findStore) {
            throw new Error("Store not found");
        }
        console.log(req.params.id);
        const data = {
            storeId: req.params.id,
            itemName: req.body.itemName,
            itemPrice: req.body.itemPrice
        };
        await Model.Product.create(data);
        return res.status(200).send("product added");
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// Update products status and prices,names etc
module.exports.updateProduct = async (req, res) => {
    try {

        // you have to pass both the id of store and product 
        const {
            storeIdq,
            prodIdq
        } = parseInt(req.query);
        const storeId = storeIdq;
        const prodId = prodIdq;
        if (!storeId || !prodId) {
            throw new Error("one or both store/product id not given in query");
        }
        const store = await Model.Product.findOne({
            storeId: storeId,
            _id: prodId
        });
        if (!store) {
            throw new Error("product not found");
        }
        const result = await Model.Product.findByIdAndUpdate(prodId, req.body, {
            new: true
        });
        //const result = await student.findById(_id);
        if (!result) {
            throw new Error("wrong id of item");
        }
        console.log("result is: ", result);
        return res.status(200).send(result);

    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
};