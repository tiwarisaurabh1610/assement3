const Model = require("../../../models/index");
const Auth = require("../../../common/authenticate");
//const services = require("../../../services/index");
const bcrypt = require('bcryptjs');
const validater = require('../../validations');


// user signup
module.exports.insertUser = async (req, res) => {
    try {
        const {
            error,
            value
        } = validater.User.userValid.validate(req.body);
        if (error) {
            console.log(error.message || error);
            return res.status(400).send(error.message || error);
        }
        console.log(value);

        const existingUserByEmail = await Model.User.findOne({
            email: req.body.email,
            isDeleted: false
        });
        const existingUserByMobile = await Model.User.findOne({
            dialCode: req.body.dialCode,
            mobileNo: req.body.mobileNo,
            isDeleted: false
        });
        if (existingUserByEmail || existingUserByMobile) {
            return res.status(400).send("Already registered with the given email or number");
        }

        const data = await Model.User.create(req.body);
        console.log(data);

        process.emit('signUp', data); // schedule welcome email on signup
        return res.status(200).send(data);
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
};

// ***** User Login function and jwt token created ******
module.exports.userLogin = async (req, res) => {
    try {
        console.log("Hello inside login");
        const {
            error,
            value
        } = validater.User.loginValid.validate(req.body);
        console.log(value);
        if (error) {
            console.log(error);
            return res.status(400).send(error.details.map(detail => detail.message));
        }
        const {
            email,
            password,
            mobileNo
        } = req.body;
        let user = null;
        if (email) {
            user = await Model.User.findOne({
                email: email,
                isDeleted: false
            }).select('password');
        } else if (mobileNo) {
            user = await Model.User.findOne({
                mobileNo: mobileNo,
                isDeleted: false
            }).select('password');
        } else {
            throw new Error("Email or mobile number is required");
        }
        if (user == null) {
            throw new Error("Oops user not found");
        }

        const isMatch = await bcrypt.compare(password, user.password);
        console.log(password);
        console.log(user.password);
        console.log(isMatch);
        if (isMatch) {
            user.jti = (Math.floor(Math.random() * 9000000000) + 1000000000).toString();
            console.log("jit is:", user.jti);
            await user.save(); 
            ////  here jwt token passed as generateToken as returned by function
            const token = Auth.generateToken({
                "id": user._id,
                role: "User"
            });
            return res.status(200).json({
                message: "Successful login",
                "token": token
            });
        } else {
            return res.status(401).send("Wrong password");
        }
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// Retreive profile 
module.exports.getProfile = async (req, res) => {
    try {
        let doc = await Model.User.findOne({
            _id: req.params.id,
            isDeleted: false
        });
        return res.status(200).send(doc);
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// Update profile
module.exports.updateProfile = async (req, res) => {
    try {
        const {
            err,
            value
        } = validater.User.updateValid.validate(req.body);
        if (err) {
            throw new Error("joi validation error");
        }
        console.log(value, "requested body");
        const _id = req.params.id;
        const result = await Model.User.findByIdAndUpdate(_id, value, {
            new: true
        });
        //const result = await student.findById(_id);
        console.log("result is: ", result);
        return res.status(200).send(result);

    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
};

// DEACTIVATE PROFILE
module.exports.deleteProfile = async (req, res) => {
    try {
        await Model.User.findOneAndUpdate({
            _id: req.params.id
        }, {
            $set: {
                isDeleted: true
            }
        }, {
            new: true
        });
        return res.status(200).send("profile deactivated");
    } catch (err) {
        console.log(err.message || err);
        return res.status(400).send(err.message || err);
    }
};

// to see the list of all stores
module.exports.getStores = async (req, res) => {
    try {
        const {
            page,
            size
        } = parseInt(req.query); // pagination 
        const limit = size || 5;
        const skip = (page - 1) * limit || 0;

        //let doc = await Model.Store.find({isDeleted: false});
        let doc = await Model.Store.aggregate([{
            $skip: skip
        }, {
            $limit: limit
        }]);
        return res.status(200).send(doc);
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// Browsing menu of a particular store
module.exports.getMenu = async (req, res) => {
    try {
        const user = await Model.User.findOne({
            _id: req.params.id,
            isDeleted: false
        });
        if (!user) {
            throw new Error("Invalid user");
        }
        let doc = await Model.Product.find({
            storeId: req.params.id,
            isAvailable: true // to get only avaialable items
        });
        return res.status(200).send(doc);
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// placing order 
module.exports.placeOrder = async (req, res) => {
    try {
        const user = await Model.User.findOne({
            _id: req.params.id,
            isDeleted: false
        });
        if (!user) {
            throw new Error("Invalid user");
        }
        validater.Order.orderPlace.validate(req.body);

        const data = await Model.Order.create({
            userId: req.params.id,
            ...req.body
        });
        return res.status(200).send(data);
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};

// to view history of orders of a user
module.exports.orderHistory = async (req, res) => {
    try {
        const user = await Model.User.findOne({
            _id: req.params.id,
            isDeleted: false
        });
        if (!user) {
            throw new Error("Invalid user");
        }
        const data = await Model.Order.find({
            userId: req.params.id
        });
        if (!data) {
            throw new Error("user is not found");
        }
        return res.status(200).send(data);
    } catch (err) {
        return res.status(400).send(err.message || err);
    }
};