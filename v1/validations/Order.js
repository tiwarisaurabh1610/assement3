const joi = require('joi');

// on signup
module.exports.orderPlace=joi.object({
    storeId:joi.required(),
    items:joi.array().required(),
    paymentMode:joi.string().required(),
    totalPrice:joi.number().required()
});