module.exports = {
    User: require("./User"),
    Store: require('./Store'),
    Order: require('./Order'),
    Product: require('./Product')
};