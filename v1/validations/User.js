const joi = require('joi');

// on signup
module.exports.userValid=joi.object({
    fName:joi.string().required(),
    lName:joi.string().required(),
    image:joi.string(),
    email:joi.string().email().required(),
    dialCode: joi.string().required(),
    mobileNo:joi.string().pattern(new RegExp('^[0-9]{10}$')).required(),
    password:joi.string().min(8).required(),
    jti:joi.string()
});

//onlogin
module.exports.loginValid = joi.object({
    email: joi.string().email(),
    dialCode: joi.string(), //  
    mobileNo: joi.string(), // 
    password: joi.string().min(8).required()
}).xor('email', 'mobileNo').with('mobileNo', 'dialCode');

//update 
module.exports.updateValid = joi.object({
    fName: joi.string(),
    lName: joi.string(),
    email: joi.string().email(),
    dialCode: joi.string().valid('+91'),
    mobileNo: joi.string().pattern(new RegExp('^[0-9]{10}$')),
    password: joi.string().min(8)
}).min(1);