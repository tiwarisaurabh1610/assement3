const joi = require('joi');

// on signup
module.exports.storeSignup = joi.object({
    storeName: joi.string().required(),
    Location: joi.string().required(),
    image: joi.string(),
    email: joi.string().email().required(),
    dialCode: joi.string().required(),
    mobileNo: joi.string().pattern(new RegExp('^[0-9]{10}$')).required(),
    password: joi.string().min(8).required(),
    jti: joi.string()
});

//onlogin
module.exports.storeLogin = joi.object({
    email: joi.string().email(),
    dialCode: joi.string(), //  
    mobileNo: joi.string(), // 
    password: joi.string().min(8).required()
}).xor('email', 'mobileNo').with('mobileNo', 'dialCode');

//update 
module.exports.storeUpdate = joi.object({
    storeName: joi.string().required(),
    Location: joi.string().required(),
    email: joi.string().email().required(),
    dialCode: joi.string().required(),
    mobileNo: joi.string().pattern(new RegExp('^[0-9]{10}$')).required(),
    password: joi.string().min(8).required()
}).min(1);