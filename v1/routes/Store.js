const Controller = require('../controller');
const Auth = require("../../common/authenticate");
const router = require('express').Router();


router.post("/signup", Controller.StoreController.insertStore);
router.post("/login", Controller.StoreController.storeLogin);
router.post("/createProduct/:id", Auth.verifyToken, Controller.StoreController.createProducts);
router.put("/updateProduct", Auth.verifyToken, Controller.StoreController.updateProduct); //pass the id of store and product in query


module.exports = router;