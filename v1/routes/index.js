const router = require("express").Router();
const UsersRoutes = require("./User");
const StoreRoutes = require("./Store");

router.use("/Store", StoreRoutes);
router.use("/User", UsersRoutes);

module.exports = router;