const Controller = require('../controller');
const Auth = require("../../common/authenticate");
const router = require('express').Router();


//Onboarding
router.post("/signup", Controller.UserController.insertUser);
router.post("/login", Controller.UserController.userLogin);
router.get("/getProfile/:id", Auth.verifyToken, Controller.UserController.getProfile);
router.put("/updateProfile/:id", Auth.verifyToken, Controller.UserController.updateProfile);
router.delete("/deactivate/:id", Auth.verifyToken, Controller.UserController.deleteProfile);


// food ordering process
router.get('/getStores',Auth.verifyToken, Controller.UserController.getStores);
router.get('/getMenu/:id',Auth.verifyToken, Controller.UserController.getMenu);
router.post('/placeOrder/:id',Auth.verifyToken, Controller.UserController.placeOrder);
router.get('/orderHistory/:id',Auth.verifyToken, Controller.UserController.orderHistory);
module.exports = router;